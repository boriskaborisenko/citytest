import {q} from './q.js'

let DEF_TEST = '1st test'

const quizText = document.querySelector('#quizText')
const answs = document.querySelector('#answs')
const next = document.querySelector('#nextQuestion')
const mainbox = document.querySelector('.mainbox')
const tbox = document.querySelector('#t_box')
const res = document.querySelector('#res')
const res_box = document.querySelector('#res_box')
const restartMe = document.querySelector('#restart')
const testName = document.querySelector('#testName')

let currentQuestion = 0
let totalQuestions = 0
let rightAnswers = 0
let selectedTest = false
let questions = ''

const selectTest = (name) => {
    mainbox.style.visibility = 'visible'
    selectedTest = q.find(x=> x.name === name)
    tbox.style.display = 'block'
    res_box.style.display = 'none'
    testName.innerHTML = selectedTest.name
}

const testQuestions = (quiz) => {
    questions = quiz.questions
    totalQuestions = quiz.questions.length
}

const pastQuestion = (number) => {
    
    const quiz = questions[number]
    const quizTitle = quiz.quest
    quizText.innerHTML = quizTitle
    
    answs.innerHTML = ''
    next.style.visibility = 'hidden'
    
    let answers = quiz.answers
    answers = answers.sort(() => Math.random() - 0.5)
    
    answers.map((a, index) => {
        let right = false
        const answ = Object.entries(a)
        if(answ.length > 1){
            right = true
        }

        const line = `<div class="line"><input  type="radio" class="op" id="var_${index}" name="x" value="${right}">
            <label id="var_${index}_lab" for="var_${index}">${answ[0][1]}</label>
            </div>`
        //console.log(quizTitle, answ[0][0], answ[0][1], right)
        answs.innerHTML += line
    })

    createActions()
    
}

const createActions = () => {
    [...document.querySelectorAll('.op')].forEach(
        action => {
            action.addEventListener('click',()=>{
                //console.log('click', action.value)
                if(action.value === 'true'){
                    rightAnswers++
                    document.querySelector(`#${action.id}_lab`).style.color = 'green'
                }else{
                    document.querySelector(`#${action.id}_lab`).style.color = 'red'
                }
                [...document.querySelectorAll('.op')].forEach(vx => {
                    vx.disabled = true
                    if(vx.value === 'true'){
                        document.querySelector(`#${vx.id}_lab`).style.color = 'green' 
                    }
                })
                currentQuestion++
                next.style.visibility = 'visible'
                //console.log(totalQuestions, currentQuestion)
                
                //console.log(rightAnswers,'RIGHT ANSWERS', currentQuestion )
            }, false)
        }
    )
    
    
}

const endTest = () => {
    //console.log('endTest')
    tbox.style.display = 'none'
    res_box.style.display = 'block'
    res.innerHTML = `Correct: ${rightAnswers}/${totalQuestions} <span>( ${(rightAnswers/totalQuestions*100).toFixed(2)} %)</span>`
}


const goTest = (name) => {
    selectTest(name)
    testQuestions(selectedTest)
    pastQuestion(currentQuestion)
}

next.addEventListener('click',()=>{
    //console.log(totalQuestions, currentQuestion)
    if(totalQuestions === currentQuestion){
        endTest()
    }else{
        pastQuestion(currentQuestion)
    }
    
}, false)

restartMe.addEventListener('click',()=>{
    currentQuestion = 0
    totalQuestions = 0
    rightAnswers = 0
    //selectedTest = false
    //questions = ''
    //console.log(selectedTest)
    answs.innerHTML = ''
    goTest(selectedTest.name)
})

document.querySelectorAll('li').forEach(li=>{
    li.addEventListener('click', ()=>{
        DEF_TEST = li.getAttribute('data-attr')
        currentQuestion = 0
        totalQuestions = 0
        rightAnswers = 0
        selectedTest = false
        questions = ''
        goTest(DEF_TEST)
    }, false)
})

















