export const q = [
    {name:'1st test',
        questions:[
            {
                num:1,
                quest:"How old are you?",
                answers:[
                    {a:'6'},
                    {b:'10'},
                    {c: '16'},
                    {d: '18', is:true}
                ]
            },
            {
                num:2,
                quest:"What is your dog name?",
                answers:[
                    {a:'Billy'},
                    {b:'Joe', is:true},
                    {c: 'Bark'},
                    {d: 'Stan'}
                ]
            }
        ]
    },
    {name:'2nd test',
        questions:[
            {
                num:1,
                quest:"2 plus 2?",
                answers:[
                    {a:'0'},
                    {b:'2'},
                    {c: '8'},
                    {d: '4', is:true}
                ]
            },
            {
                num:2,
                quest:"Monaco is?",
                answers:[
                    {a:'Name'},
                    {b:'City', is:true},
                    {c: 'Car'},
                    {d: 'Ship'}
                ]
            },
            {
                num:3,
                quest:"A new question?",
                answers:[
                    {a:'Wrong'},
                    {b:'Right', is:true},
                    {c: 'Wrong'},
                    {d: 'Wrong'},
                    {e: 'WRONG'}
                ]
            }
        ]
    }

]